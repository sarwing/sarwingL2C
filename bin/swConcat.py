#!/usr/bin/env python

from __future__ import print_function
import logging
import argparse
import os
import sys
import datetime
import socket
import sarwingL2C

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


description = "Concatenate netCDF files"
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-d", "--outDir", action="store", default=".", nargs=1, type=str,help="output directory with filename or only output directory if --autoname is set")
parser.add_argument("-i", "--files", action="append", nargs="+", type=str, help="netCDF (.nc) files to concatenate.")
parser.add_argument("-a", "--autoName", action="store_true", default=False, help="If this flag is set, the script generates the output filename itself. ")
parser.add_argument("--debug", action="store_true", default=False, help="start the script in debug mode")
parser.add_argument("-n", "--noHarmonize", action="store_true", default=False, help="disable the harmonization of images range. If they have different ranges, the script may fail.")
parser.add_argument("-v", "--version", action="store_true", default=False, help="Returns the script version")

args = parser.parse_args()

if args.version:
    print(sarwingL2C.swConcatProcessor.version)
    sys.exit(0)
    
logger.info("Script executed at %s" % datetime.datetime.now())
logger.info("Called with the following command : %s" % " ".join(sys.argv))
logger.info("The host machine is : %s" % socket.gethostname())
    
if args.debug:
    try:
        import debug
    except:
        pass

if not os.path.exists(os.path.dirname(args.outDir[0])):
    try:
        os.makedirs(os.path.dirname(args.outDir[0]))
    except:
        pass

status=3

status=sarwingL2C.swConcatProcessor.netCDFConcat(args.files[0], args.outDir[0], args.autoName, args.noHarmonize)
    
sys.exit(status)
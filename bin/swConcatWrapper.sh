#!/bin/bash

dir=`dirname $1`

mkdir -p "$dir"

swConcat.py -d $1 -i "${@:2}" > "$dir/Concat.log" 2>&1
status=$?
echo $status > "$dir/Concat.status"

swConcat.py -v > "$dir/Concat.version"

cat "$dir/Concat.log"

exit $status
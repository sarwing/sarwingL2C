#!/usr/bin/env python
# -*- coding: latin-1 -*-

from builtins import str
import logging
import argparse
import datetime
import sys
import os
import socket
import sarwingL2C

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

logger.info("Script executed at %s" % datetime.datetime.now())
logger.info("Called with the following command : %s" % " ".join(sys.argv))
logger.info("The host machine is : %s" % socket.gethostname())

description = """Reads paths line by line from stdin, groups the paths with same takeId on one line, prefixed with the proposed output file. 
The existing files are not recreated, they are skipped by default."""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-d", "--outDir", action="store", default=".", type=str,help="Base output directory (other directories will be created inside)")
parser.add_argument("-f", "--force", action="store_true", default=False, help="Forces existing files to be recreated.")
parser.add_argument("--debug", action="store_true", default=False, help="start the script in debug mode")
parser.add_argument("--reportPath", action="store", default=None, help="Specify the path and filename where to record the created files")

args = parser.parse_args()

if args.debug:
    try:
        import debug
    except:
        pass
    
l2path = sys.stdin.readlines()    
try:
    sarwingL2C.swGroupProcessor.createConcat(l2path,outDir=args.outDir,force=args.force,reportPath=args.reportPath,debug=args.debug)
except Exception as e:
    logger.exception("Exception : %s", str(e) )
    sys.exit(1)
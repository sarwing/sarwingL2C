# -*- coding: latin-1 -*-

# Script callable to concatenate several NetCDF files
# Script callable to regroup and concatenate several groups of NetCDF files
# Garder les scripts bash ? -> ils génèrent status et log, indispensables pour le moment

from __future__ import unicode_literals
from .swConcatProcessor import *
from .swGroupProcessor import *

# -*- coding: latin-1 -*-

from __future__ import division
from __future__ import unicode_literals
from builtins import map
from builtins import str
from builtins import range
from past.utils import old_div
import netCDF4
import numpy as np
from shutil import copyfile
import logging
import datetime
import os
import sys
from collections import OrderedDict
import argparse
from numpy import number
import faulthandler
from math import copysign
from shapely.wkt import dumps, loads
import socket
import git
from ._version import __version__

version = __version__

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


# Copy the groups of the netCDF file source to the netCDF file dest
def copyGroups(ncSource, ncDest):
    logger.debug("Copying groups...")
    for groupName in [None] + list(ncSource.groups.keys()):
        if groupName:
            ncDest.createGroup(groupName)
    logger.debug("Groups copied")


# Copy the dimensions of the netCDF source to the netCDF file dest
#  Parameter azDimToSet will be set for dimension called owiAzSize and
# parameter maxRaDimToSet will be set for dimension called owiRaSize
def copyDims(ncSource, ncDest, azDimToSet, maxRaDimToSet):
    logger.debug("Copying dims...")
    # Copy the dimension of the netCDF file 1
    for groupName in [None] + list(ncSource.groups.keys()):
        if groupName:
            group = ncSource.groups[groupName]
            destGroup = ncDest[groupName]
        else:
            group = ncSource
            destGroup = ncDest

        for dimName, dim in list(group.dimensions.items()):
            val = len(dim)

            if dimName == "owiAzSize":
                val = azDimToSet
            if dimName == "owiRaSize":
                val = maxRaDimToSet

            destGroup.createDimension(dimName, val)
    logger.debug("Copied dims.")


# Create the variables and attributes of the netCDF source into the netCDF file dest
def copyVars(ncSource, ncDest):
    logger.debug("Copying vars...")
    for groupName in [None] + list(ncSource.groups.keys()):
        if groupName:
            group = ncSource.groups[groupName]
            destGroup = ncDest[groupName]
        else:
            group = ncSource
            destGroup = ncDest

        for ncVarName, ncVar in list(group.variables.items()):
            fillValue = None
            if "_FillValue" in ncVar.ncattrs():
                fillValue = getattr(ncVar, "_FillValue")

            var = destGroup.createVariable(
                ncVarName, ncVar.dtype, ncVar.dimensions, fill_value=fillValue
            )
            # Proceed to copy the variable attributes
            for attName in ncVar.ncattrs():
                if attName == "dtype":  # FIXME
                    continue
                if attName != "_FillValue":
                    setattr(var, attName, getattr(ncVar, attName))
    logger.debug("Copied vars.")


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance and bearing between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = list(map(np.radians, [lon1, lat1, lon2, lat2]))

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = (
        np.sin(old_div(dlat, 2)) ** 2
        + np.cos(lat1) * np.cos(lat2) * np.sin(old_div(dlon, 2)) ** 2
    )
    c = 2 * np.arcsin(np.sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    bearing = np.arctan2(
        np.sin(lon2 - lon1) * np.cos(lat2),
        np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(lon2 - lon1),
    )
    return c * r, np.rad2deg(bearing)


# Return the maximum distance found between two adjacent slices among all slices.
def checkDistances(varDict):
    dists = []
    for i in range(len(varDict) - 1):
        middle = old_div(varDict[i]["/"]["owiLon"].shape[1], 2)

        dists.append(
            haversine(
                varDict[i]["/"]["owiLon"][-1, middle],
                varDict[i]["/"]["owiLat"][-1, middle],
                varDict[i + 1]["/"]["owiLon"][0, middle],
                varDict[i + 1]["/"]["owiLat"][0, middle],
            )[0]
        )

    if len(dists) != 0:
        return max(dists)
    return 0


# Check for shift between adjacent slices and realign them.
def harmonizeColumns(varDict, noHarmonize):
    status = 0
    logger.debug("Shifting misaligned columns...")

    # positive shift -> must shift to the right
    for i in range(0, len(varDict) - 1):
        middle = old_div(varDict[i]["/"]["owiIncidenceAngle"].shape[1], 2)
        refDist = varDict[i]["/"]["owiLon"][-1, middle]

        dists = haversine(
            refDist,
            varDict[i]["/"]["owiLat"][-1, middle],
            varDict[i + 1]["/"]["owiLon"][0, middle - 50 : middle + 50],
            varDict[i + 1]["/"]["owiLat"][0, middle - 50 : middle + 50],
        )

        idxDist = (dists[0]).argmin()

        minIndexAbsDist = middle + (idxDist - (old_div(len(dists[0]), 2)))

        shift = middle - minIndexAbsDist

        if abs(shift) > 100:
            logger.error(
                "Shift (value : %s) is too big between these two images : image number %s and number %s. Setting error state."
                % (shift, i, i + 1)
            )
            status = 3

        if noHarmonize:
            shift = 0  # Disable the shift

        # If the shift is negative (shifting the second image to the left) : must create columns at the left of the first image (disabled)
        # or must delete columns at the left of the second image
        # But we must take care of the right side too
        if shift < 0:
            # Here we must resize all the images before the one we are shifting
            # Create column left for the previous images (disabled)
            logger.debug("Shift is %s. Shifting..." % shift)
            # addColumnsToVars(varDict[0:i+1], shift)
            # Remove column left of second image
            removeColumnsToVars([varDict[i + 1]], shift)
            logger.debug("Successfully shifted.")

        elif shift > 0:
            # If the shift is positive (shifting the second image to the right) : must create columns at the left of the second image (disabled)
            # or must delete columns on left of first image
            # But we must take care of the right side too
            ##Create column left second image (disabled)
            logger.debug("Shift is %s. Shifting..." % shift)
            # addColumnsToVars([varDict[i+1]], -shift)
            # Deletes columns left from previous images
            removeColumnsToVars(varDict[0 : i + 1], -shift)
            logger.debug("Successfully shifted.")

        # Check, after adding/removing the columns, which image is the largest
        # (disabled) Create new columns on the right side on the smallest image (disabled)
        # Delete columns on the right side on the largest image
        diffSize = (
            varDict[i + 1]["/"]["owiIncidenceAngle"].shape[1]
            - varDict[i]["/"]["owiIncidenceAngle"].shape[1]
        )
        if diffSize > 0:
            # (disabled)Create columns on the previous images at the right(disabled)
            # (disabled)Here we must resize all the images before the one we are shifting(disabled)
            # logger.debug("Adding columns to previous images...")
            # addColumnsToVars(varDict[0:i+1], diffSize)

            # Deletes columns on the second image at the right
            logger.debug("Removing columns to second image...")
            removeColumnsToVars([varDict[i + 1]], diffSize)
            logger.debug("Successfully removed columns.")
        elif diffSize < 0:
            # (disabled)Create columns the second image at the right(disabled)
            # logger.debug("Adding columns to previous images...")
            # addColumnsToVars([varDict[i+1]], -diffSize)

            # Deletes columns on the previous images on the right
            logger.debug("Removing columns to previous images...")
            removeColumnsToVars(varDict[0 : i + 1], -diffSize)
            logger.debug("Successfully removed columns.")

    logger.debug("Finished shifting with status %s" % status)
    return status


# Adds columns to the left or right side of the data in varDicts
# numberOfColumns negative --> add the columns at the left
def addColumnsToVars(varDicts, numberOfColumns):
    for dict in varDicts:
        for groupName in dict:
            for varName in dict[groupName]:
                var = dict[groupName][varName]
                if numberOfColumns < 0:
                    if varName == "owiMask":
                        dict[groupName][varName] = np.hstack(
                            [np.full([var.shape[0], abs(numberOfColumns)], 3), var]
                        )
                    else:
                        dict[groupName][varName] = np.hstack(
                            [np.full([var.shape[0], abs(numberOfColumns)], np.nan), var]
                        )
                elif numberOfColumns > 0:
                    if varName == "owiMask":
                        dict[groupName][varName] = np.hstack(
                            [var, np.full([var.shape[0], abs(numberOfColumns)], 3)]
                        )
                    else:
                        dict[groupName][varName] = np.hstack(
                            [var, np.full([var.shape[0], abs(numberOfColumns)], np.nan)]
                        )


# Removes columns to the left or right side of the data in varDicts
# numberOfColumns negative --> remove the columns at the left
def removeColumnsToVars(varDicts, numberOfColumns):
    for dict in varDicts:
        for groupName in dict:
            for varName in dict[groupName]:
                var = dict[groupName][varName]
                if numberOfColumns < 0:
                    dict[groupName][varName] = np.delete(
                        var, np.s_[0:-numberOfColumns], 1
                    )
                elif numberOfColumns > 0:
                    dict[groupName][varName] = np.delete(
                        var, np.s_[-numberOfColumns - 1 : -1], 1
                    )


# Check each pair of slice for overlapping. If overlapping is detected, the
# data with better quality will be kept and all other redundant data will
# be removed
def manageOverlap(varDicts, footprints):
    logger.debug("Checking for overlap...")
    limArray = []
    # The first index selects the first acquired slice
    if len(footprints) >= 2:
        for i in range(len(footprints) - 1):
            slice1Polyg = loads(footprints[i])
            slice2Polyg = loads(footprints[i + 1])

            if slice1Polyg.intersects(slice2Polyg):
                logger.debug(
                    "Slice %s and %s overlap. Searching overlap limit indexes..."
                    % (i, i + 1)
                )
                slice1PolygCoords = slice1Polyg.exterior.coords.xy
                # 'oldest' or 'youngest' points means the points that for the same line has been acquired first or last
                slice1LeftTop = (
                    slice1PolygCoords[0][-2],
                    slice1PolygCoords[1][-2],
                )  # Selecting the first slice junction oldest point
                slice1RightTop = (
                    slice1PolygCoords[0][-3],
                    slice1PolygCoords[1][-3],
                )  # Selecting the first slice junction youngest point

                slice2PolygCoords = slice2Polyg.exterior.coords.xy
                slice2LeftBot = (
                    slice2PolygCoords[0][0],
                    slice2PolygCoords[1][0],
                )  # Selecting the second slice junction oldest point
                slice2RightBot = (
                    slice2PolygCoords[0][1],
                    slice2PolygCoords[1][1],
                )  # Selecting the slice junction youngest point

                if slice2LeftBot[1] > slice1PolygCoords[1][0]:
                    direction = "asc"
                else:
                    direction = "desc"

                # Now that we have the intersecting Polygon coordiantes (lon/lat), we need to find the corresponding data index in each slice
                # To do that, I'll iterate from the the junction of slice1 to the other side until I found a coord outside of
                # the intersecting Polygon
                # As a reminder the data are 2D mapped as following:
                # [heightCoord,widthCoord] -> heightCoord = 0 selects the row at the slice junction opposite (for the first slice)
                # -> widthCoord = 0 selects a column at the slice oldest side (the side where the data are first acquired for each line)
                # So for slice1 I'll iterate from the junction oldest and youngest side so from:
                # slice1Data[-1,0] and slice1Data[-1,-1]
                # For slice2 I'll iterate from the junction oldest and youngest side so from:
                # slice2Data[0,0] and slice2Data[0,-1]
                # Also keep in mind that data can be masked so this can be problematic to verify
                # that the current row is inside or outside our intersection Polygon
                # A workaround for this is to select two valid points of the row and
                # compute the line
                y = -1
                while True:
                    xL = 0
                    xR = -1
                    maskz = varDicts[i]["/"]["owiMask"][y, :]
                    # While the value is masked, search another one on the same row
                    while (
                        varDicts[i]["/"]["owiMask"][y, xL] == 3
                        and xL < varDicts[i]["/"]["owiMask"].shape[1]
                    ):
                        xL += 1
                    while (
                        varDicts[i]["/"]["owiMask"][y, xR] == 3
                        and (abs(xR) - 1) < varDicts[i]["/"]["owiMask"].shape[1]
                    ):
                        xR -= 1
                    # The points must be different.. Go on the next line
                    if xL == xR:
                        y -= 1
                        continue
                    curCoordsLeft = (
                        varDicts[i]["/"]["owiLon"][y, xL],
                        varDicts[i]["/"]["owiLat"][y, xL],
                    )
                    curCoordsRight = (
                        varDicts[i]["/"]["owiLon"][y, xR],
                        varDicts[i]["/"]["owiLat"][y, xR],
                    )
                    # Compute line factor
                    lineFactor = old_div(
                        (curCoordsRight[1] - curCoordsLeft[1]),
                        (curCoordsRight[0] - curCoordsLeft[0]),
                    )
                    k = curCoordsLeft[1] - lineFactor * curCoordsLeft[0]
                    compLat = slice2LeftBot[0] * lineFactor + k
                    if direction in "asc":
                        if compLat < slice2LeftBot[1]:
                            break
                    else:
                        if compLat > slice2LeftBot[1]:
                            break
                    y -= 1

                slice1Limit = (
                    y + 2
                )  # Index on slice1 delimiting the overlapping. Empirically/visually, the +2 is the best choice.
                slice2Limit = -y - 1  # Index on slice2 delimiting the overlapping
                # limArray.append((slice1Limit, slice2Limit))
                logger.info(
                    "Slice %s and %s overlap by %s 'pixels'"
                    % (i, i + 1, slice1Limit - 1)
                )
                # logger.info("(%s, %s) (%s, %s)" % (varDicts[i]['/']['owiLon'][y, 0], varDicts[i]['/']['owiLat'][y, 0], varDicts[i]['/']['owiLon'][y, -1], varDicts[i]['/']['owiLat'][y, -1]))
                # logger.info("(%s, %s) (%s, %s)" % (varDicts[i + 1]['/']['owiLon'][0, 0], varDicts[i + 1]['/']['owiLat'][0, 0], varDicts[i + 1]['/']['owiLon'][0, -1], varDicts[i + 1]['/']['owiLat'][0, -1]))
                # logger.info("(%s, %s) (%s, %s)" % (varDicts[i + 1]['/']['owiLon'][-1, 0], varDicts[i + 1]['/']['owiLat'][-1, 0], varDicts[i + 1]['/']['owiLon'][-1, -1], varDicts[i + 1]['/']['owiLat'][-1, -1]))

                # logger.info("(%s, %s) (%s, %s)" % (varDicts[i]['/']['owiLon'][y + 1, 0], varDicts[i]['/']['owiLat'][y + 1, 0], varDicts[i]['/']['owiLon'][y + 1, -1], varDicts[i]['/']['owiLat'][y + 1, -1]))
                # logger.info("(%s, %s) (%s, %s)" % (varDicts[i + 1]['/']['owiLon'][y + 1, 0], varDicts[i + 1]['/']['owiLat'][y + 1, 0], varDicts[i + 1]['/']['owiLon'][y + 1, -1], varDicts[i + 1]['/']['owiLat'][y + 1, -1]))

                # A priori there's no need to compute the other limit because they are the
                # same (but opposed)

                logger.debug("Keeping best quality pixels...")
                # For chosing quality we use the variable Filter_2 (old), or  owiWindFilter (new)
                # TODO: check that the variable exists ?
                # if "Filter_2" in varDicts[i]["owiPreProcessing/"]:
                #    filter_grp = "owiPreProcessing/"
                #    filter_var =  "Filter_2"
                # else:
                #    filter_grp = "/"
                #    filter_var = "owiWindFilter"
                #    #float Filter_2(owiAzSize, owiRaSize) ;
                # Filter_2:long_name = "Filter matrix at 400m-pixel with 0 = non usable pixels" ;
                # For each index, check the best Filter_2 value (close to 1) to select which best data to keep
                # for i in range(len(varDicts) - 1):
                # for y in range(-1, slice1Limit, -1):
                #    for x in range(varDicts[i][filter_grp][filter_var].shape[1]):
                #        filterVal1 = varDicts[i][filter_grp][filter_var][y, x]
                #        filterVal2 = varDicts[i + 1][filter_grp][filter_var][slice2Limit + y, x]
                #        if filterVal1 < filterVal2:
                #            #Iterate through each var to copy pixel from slice 2 to slice 1
                #            for groupName, group in list(varDicts[i].items()):
                #                for varName, var in list(group.items()):
                #                    varDicts[i][groupName][varName][y, x] = varDicts[i + 1][groupName][varName][slice2Limit + y, x]
                # if varName == "owiWindSpeed_Tab_dualpol_2steps":
                #   varDicts[i][groupName][varName][y, x] = 0

                # Deleting redundant data
                for groupName, group in list(varDicts[i + 1].items()):
                    for varName, var in list(group.items()):
                        varDicts[i + 1][groupName][varName] = np.delete(
                            var, np.s_[:slice2Limit], axis=0
                        )
            else:
                logger.debug(
                    "No overlap detected between slice %s and %s." % (i, i + 1)
                )
    else:
        logger.debug("Only 1 slice available, overlap is impossible.")
    logger.debug("Overlap processing over.")


# @param fnames array of string containing filenames
def netCDFConcat(fnames, outputFullPath, autoName, noHarmonize):
    status = 3
    logger.info(
        "Starting concatenation of the following files : %s into %s."
        % (fnames, outputFullPath)
    )
    faulthandler.enable()

    fnames = reorderFiles(fnames)

    if autoName:
        outputFullPath = outputFullPath + "/" + generateFilename(fnames)

    if not os.path.exists(os.path.dirname(outputFullPath)):
        try:
            os.makedirs(os.path.dirname(outputFullPath))
        except:
            pass
    logger.info("Output path and filename : %s" % (outputFullPath))

    base = os.path.splitext(outputFullPath)[0]
    tempFilename = base + ".tmp.nc"

    logger.debug("Concat file successfully created.")

    dimensionAzSum = 0
    ranges = []

    logger.debug("Reading input files dimensions...")

    # read all the files' dimensions and add them
    for fname in fnames:
        try:
            nc = netCDF4.Dataset(fname)
        except Exception as e:
            raise IOError("skipping unreadable %s : %s. Exiting" % (fname, str(e)))

        dimensionAzSum += nc.dimensions["owiAzSize"].size

        nc.close()

    logger.debug(
        "Successfully read files dimensions, dimensionAzSum = %s" % dimensionAzSum
    )

    logger.debug("Reading and storing files data...")

    footprints = []
    concatDict = {}
    varDicts = []
    globAttr = {}
    # Store the variables
    for i in range(0, len(fnames)):
        try:
            nc = netCDF4.Dataset(fnames[i])
        except Exception as e:
            raise IOError("skipping unreadable %s : %s. Exiting" % (fnames[i], str(e)))

        varDicts.append({})

        if "sourceProduct" not in globAttr:
            globAttr["sourceProduct"] = getattr(nc, "sourceProduct")
        else:
            globAttr["sourceProduct"] += " %s" % getattr(nc, "sourceProduct")

        # read groups
        for groupName in [None] + list(nc.groups.keys()):
            if groupName:
                group = nc.groups[groupName]
                groupNamePref = "%s/" % groupName
            else:
                group = nc
                groupNamePref = "/"

            if groupNamePref not in varDicts:
                varDicts[i][groupNamePref] = {}

            for varName in group.variables:
                # FIXME for now those 2 vars are skipped, but it is better to concatenated them and set them as
                #  dimensions in the resulting file
                if varName == "owiAzSize" or varName == "owiRaSize":
                    continue
                var = group[varName]
                if var.dimensions[0] == "owiAzSize":
                    if varName not in varDicts[i][groupNamePref]:
                        varDicts[i][groupNamePref][varName] = var[:]

        footprints.append(getFootprint(nc))
        varDicts[i]["/"]["sliceNumber"] = np.full(
            (nc.dimensions["owiAzSize"].size, nc.dimensions["owiRaSize"].size), i
        )
        nc.close()

    allOverlap = True
    for i in range(len(footprints) - 1):
        slice1Polyg = loads(footprints[i])
        slice2Polyg = loads(footprints[i + 1])
        if not slice1Polyg.intersects(slice2Polyg):
            allOverlap = False

    maxDist = checkDistances(varDicts)
    if maxDist > 200 and not allOverlap:
        logger.error(
            "A large gap of %s has been detected between two images... A middle image may be missing. Error state forced"
            % maxDist
        )
        status = 3
    elif maxDist > 10 and not allOverlap:
        logger.warning(
            "A gap of %s has been detected between two images... The attribute 'hasGap' will be set to True. "
            % maxDist
        )
        globAttr["hasGap"] = "True"
    else:
        globAttr["hasGap"] = "False"

    logger.debug("Successfully read and stored files data.")

    status = harmonizeColumns(varDicts, noHarmonize)
    manageOverlap(varDicts, footprints)

    dimensionAzSum = 0
    for varDict in varDicts:
        dimensionAzSum += varDict["/"]["owiLon"].shape[0]

    logger.debug("Copying first file data into new file...")
    # open first netCDF file
    try:
        nc = netCDF4.Dataset(fnames[0])
    except Exception as e:
        raise IOError("skipping unreadable %s : %s. Exiting" % (fnames[0], str(e)))

    # create new file
    try:
        logger.debug("Creating concat file %s" % tempFilename)
        ncConcat = netCDF4.Dataset(tempFilename, "w")
    except Exception as e:
        raise IOError("cannot create file %s : %s. Exiting" % (outputFullPath, str(e)))

    logger.debug("Copying attributes...")
    # copy global attributes of file 1
    for attname in nc.ncattrs():
        if attname == "name":  # FIXME
            continue
        setattr(ncConcat, attname, getattr(nc, attname))
    logger.debug("Attributes copied.")

    copyGroups(nc, ncConcat)
    copyDims(
        nc, ncConcat, dimensionAzSum, varDicts[0]["/"]["owiIncidenceAngle"].shape[1]
    )
    copyVars(nc, ncConcat)
    ncConcat.createVariable("sliceNumber", "u4", ("owiAzSize", "owiRaSize"))

    logger.debug("Starting data concatenation...")
    # Concatenation
    for dict in varDicts:
        for groupName in dict:
            if groupName not in concatDict:
                concatDict[groupName] = {}
            for varName in dict[groupName]:
                if varName == "owiAzSize" or varName == "owiRaSize":  # FIXME
                    continue
                logger.debug(
                    "Concatenating var %s in group %s ..." % (varName, groupName)
                )

                var = dict[groupName][varName]
                if varName not in concatDict[groupName]:
                    concatDict[groupName][varName] = np.full([0, var.shape[1]], np.nan)

                concatVar = concatDict[groupName][varName]
                concatVar = np.concatenate((concatVar, var), axis=0)
                concatDict[groupName][varName] = concatVar

    logger.debug("Finished data concatenation.")

    # Adding new attributes
    globAttr["TITLE"] = "Sentinel-1 OWI Concatenated Component"

    outFilename = os.path.basename(outputFullPath)
    fnData = outFilename.split("-")
    startDate = datetime.datetime.strptime(fnData[4], "%Y%m%dt%H%M%S")
    stopDate = datetime.datetime.strptime(fnData[5], "%Y%m%dt%H%M%S")
    globAttr["firstMeasurementTime"] = startDate.strftime("%Y-%m-%dT%H:%M:%SZ")
    globAttr["lastMeasurementTime"] = stopDate.strftime("%Y-%m-%dT%H:%M:%SZ")
    globAttr["L2_concatenation_processor_version"] = version

    for attname, val in list(globAttr.items()):
        setattr(ncConcat, attname, val)

    logger.debug("Setting concatenated data into new .nc file...")
    # Assign the variables sum to the new netCDF file
    for groupName in [None] + list(ncConcat.groups.keys()):
        if groupName:
            concatGroup = ncConcat[groupName]
            groupNamePref = "%s/" % groupName
        else:
            concatGroup = ncConcat
            groupNamePref = "/"

        for varName in concatGroup.variables:
            if varName == "owiAzSize" or varName == "owiRaSize":  # FIXME
                continue
            var = concatGroup[varName]
            if var.dimensions[0] == "owiAzSize":
                var[:] = concatDict[groupNamePref][varName][:]

    logger.debug("Finished setting concatenated data into new .nc file.")

    addBoxAttribute(ncConcat, footprints)
    ncConcat.close()
    os.rename(tempFilename, outputFullPath)
    logger.info("Successfully concatenated files into %s." % outputFullPath)
    return status


# reorderFiles
# Takes an array of netCDF filenames in argument. These are the files that will
# be concatenated into one. They must be in the right order (depends on the
# acquisition date)
# @params fnames netCDF array of filename to reorder
def reorderFiles(fnames):
    logger.debug("Reordering .nc files...")
    dates = {}
    for fname in fnames:
        try:
            startDate = datetime.datetime.strptime(
                os.path.basename(fname).split("-")[4], "%Y%m%dt%H%M%S"
            )
        except Exception as e:
            raise ValueError(
                "Error when parsing date in filename %s. Error : %s. Exiting"
                % (fname, str(e))
            )
        if startDate in dates:
            dates[startDate].append(fname)
        else:
            dates[startDate] = [fname]

    orderedDates = OrderedDict(sorted(dates.items()))

    file_list_ordered = [f for _, fs in orderedDates.items() for f in fs]
    logger.debug("Successfully reordered files.")
    return file_list_ordered


# Not used
def checkFnConsistency(fnames):
    fs = []
    for fname in fnames:
        fs.append(os.path.basename(fname))

    fnData = fs[0].split("-")
    orbitalNum = fnData[6]
    takeId = fnData[7]
    satellite = fnData[0]
    instr = fnData[1]
    for fname in fs:
        fnData = fname.split("-")
        if (
            fnData[6] != orbitalNum
            or fnData[7] != takeId
            or fnData[0] != satellite
            or fnData[1] != instr
        ):
            return False
    return True


# generateFilename
# Takes an array of netCDF filename, previously ordered by acquisition time and
# generate a new filename that takes as a start acquisition date the first date of
# the first filename and as end acquisition date the last date of the last filename.
# @param fnames array of filenames
def generateFilename(fnames):
    fnData1 = os.path.basename(fnames[0]).split("-")
    fnData2 = os.path.basename(fnames[-1]).split("-")
    fname = "%s-%s-%s-cc-%s-%s-%s-%s" % (
        fnData1[0],
        fnData1[1],
        fnData1[2],
        fnData1[4],
        fnData2[5],
        fnData1[6],
        fnData1[7],
    )

    return fname


# Return nc footprint using well known format (Well Known Text (WKT))
# TODO maybe use shapely to create Polygon THEN use dumps function to create string
def getFootprint(ncFile):
    logger.debug("Generating bounding box polygon from nc file...")
    cl = [[0, 0], [0, -1], [-1, -1], [-1, 0], [0, 0]]
    ca = [[0, 0], [0, -1], [-1, -1], [-1, 0], [0, 0]]
    for i in range(len(cl)):
        while np.isnan(ncFile["owiLon"][cl[i][0], cl[i][1]]):
            cl[i][0] += copysign(1, cl[i][0])
            cl[i][1] += copysign(1, cl[i][1])

        while np.isnan(ncFile["owiLat"][ca[i][0], ca[i][1]]):
            ca[i][0] += copysign(1, ca[i][0])
            ca[i][1] += copysign(1, ca[i][1])

    polyg = "POLYGON((%s %s, %s %s, %s %s, %s %s, %s %s))" % (
        ncFile["owiLon"][cl[0][0], cl[0][1]],
        ncFile["owiLat"][ca[0][0], ca[0][1]],
        ncFile["owiLon"][cl[1][0], cl[1][1]],
        ncFile["owiLat"][ca[1][0], ca[1][1]],
        ncFile["owiLon"][cl[2][0], cl[2][1]],
        ncFile["owiLat"][ca[2][0], ca[2][1]],
        ncFile["owiLon"][cl[3][0], cl[3][1]],
        ncFile["owiLat"][ca[3][0], ca[3][1]],
        ncFile["owiLon"][cl[4][0], cl[4][1]],
        ncFile["owiLat"][ca[4][0], ca[4][1]],
    )
    logger.debug("Generated polygon : %s" % polyg)
    logger.debug("Finished to generate polygon.")
    return polyg


def addBoxAttribute(ncConcat, footprints):
    ncConcat.footprint = getFootprint(ncConcat)
    ncConcat.slicesFootprints = ";".join(footprints)

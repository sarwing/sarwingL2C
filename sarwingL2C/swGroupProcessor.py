from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals
from builtins import range
import sys
import os
import datetime
import argparse
import logging
import re
import csv
from .swConcatProcessor import version
import socket
from pathurl import toUrl
import collections

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def generateFullPath(paths):
    startDates = []
    endDates = []
    for path in paths:
        SAFEdir = path.split("/")[-1]
        SAFEsplit = SAFEdir.split("-")
        startDates.append(datetime.datetime.strptime(SAFEsplit[4], "%Y%m%dt%H%M%S"))
        endDates.append(datetime.datetime.strptime(SAFEsplit[5], "%Y%m%dt%H%M%S"))

    startDates.sort()
    endDates.sort()

    pathSplit = []
    for path in paths:
        if startDates[0].strftime("%Y%m%dt%H%M%S") in path:
            pathSplit = path.split("/")

    if len(pathSplit) == 0:
        logger.error("Error : no base path matching start date found...")

    if pathSplit[-2].endswith(".SAFE"):
        del pathSplit[-2]
    if "RS2" in pathSplit[-2] or "RCM" in pathSplit[-2]:
        gr = ""
    else:
        gr = "ERROR"

    grdhSplit = pathSplit[-4].split("_")
    if len(grdhSplit) > 2 and re.search("GRD\w", grdhSplit[2]):
        gr = grdhSplit[2][-1]
        grdhSplit[2] = "OWI" + grdhSplit[2][-1]
        pathSplit[-4] = "_".join(grdhSplit)

    if re.match("L\d\w?", pathSplit[-6]):
        pathSplit[-6] = "L2C"

    owiSplit = pathSplit[-1].split("-")
    owiSplit[3] = "cc"
    owiSplit[4] = startDates[0].strftime("%Y%m%dt%H%M%S")
    owiSplit[5] = endDates[-1].strftime("%Y%m%dt%H%M%S")
    owiName = "-".join(owiSplit)
    pathSplit[-1] = owiName

    newDir = (
        pathSplit[-1]
        .replace("-", "_")
        .replace(".nc", "")
        .replace("owi", "OWI" + gr)
        .upper()
    )

    pathSplit.insert(-1, newDir)

    return "/".join(pathSplit)


def reorderComments(dict):
    commentsDict = {}

    for i in range(0, len(dict["paths"])):
        SAFEdir = dict["paths"][i].split("/")[-1]
        SAFEsplit = SAFEdir.split("-")
        startDate = datetime.datetime.strptime(SAFEsplit[4], "%Y%m%dt%H%M%S")
        commentsDict[startDate] = dict["comments"][i]

    return " ".join(
        list(collections.OrderedDict(sorted(commentsDict.items())).values())
    )


def checkVersion(outDir):
    try:
        logger.debug("Trying to open version file in %s ..." % outDir)
        f = open(outDir + "/Concat.version", "r")
    except IOError:
        logger.debug("No previous version file. Generation will be forced.")
        return True
    else:
        with f:
            ver = f.readline().strip()
            if ver == version:
                logger.debug(
                    "Version file open, versions are the same : %s. Generation will not be forced"
                    % ver
                )
                return False
            else:
                logger.debug(
                    "Version file open, versions are different, previous : %s, newest : %s. Generation will be forced."
                    % (ver.strip(), version.strip())
                )
                return True


def checkStatus(outDir):
    try:
        logger.debug("Trying to open status file in %s ..." % outDir)
        f = open(outDir + "/Concat.status", "r")
    except IOError:
        logger.debug("No previous status file. Generation will be forced.")
        return True
    else:
        with f:
            status = f.readline().strip()
            if (status == "0") or (status == "3"):  # 3 is too large gap
                logger.debug("Status OK")
                return False
            else:
                logger.debug("Status is %s, generation will be forced" % status)
                return True


def createConcat(l2path, outDir=".", force=False, reportPath=None, debug=False):
    if reportPath:
        logFileHandler = logging.FileHandler(reportPath + "/swTakeIdGroup.log")
        logger.addHandler(logFileHandler)

    takeIdDict = {}
    rs2idx = 0

    for owiFileCom in l2path:
        # FIXME these 2 checks are too weak
        rs2Listing = "RS2" in owiFileCom
        rcmListing = "rcm" in owiFileCom

        owiFileCom = owiFileCom.strip()
        owiFile = owiFileCom.split(" ")[0]
        commentList = owiFileCom.split(" ")[1:]
        comment = " ".join(commentList)

        if rs2Listing:
            owiSplit = owiFile.split("/")[-1].split("-")
            owiStartDate = owiSplit[-4]
            owiEndDate = owiSplit[-3]
            startDate = datetime.datetime.strptime(owiStartDate, "%Y%m%dt%H%M%S")
            endDate = datetime.datetime.strptime(owiEndDate, "%Y%m%dt%H%M%S")
            hasGroup = False
            for idx, dict in list(takeIdDict.items()):
                closestDelta = datetime.timedelta(999999999)
                if "startDates" in dict:
                    for sD in dict["startDates"]:
                        dateDelta = abs(endDate - sD)
                        if dateDelta < closestDelta:
                            closestDelta = dateDelta
                if "endDates" in dict:
                    for eD in dict["endDates"]:
                        dateDelta = abs(eD - startDate)
                        if dateDelta < closestDelta:
                            closestDelta = dateDelta
                if closestDelta.days == 0 and abs(closestDelta.seconds) < 15:
                    dict["startDates"].append(startDate)
                    dict["endDates"].append(endDate)
                    dict["paths"].append(owiFile)
                    dict["comments"].append(comment.strip())
                    hasGroup = True
                    break
            if not hasGroup:
                takeIdDict[rs2idx] = {}
                takeIdDict[rs2idx]["startDates"] = [startDate]
                takeIdDict[rs2idx]["endDates"] = [endDate]
                takeIdDict[rs2idx]["paths"] = [owiFile]
                takeIdDict[rs2idx]["comments"] = [comment.strip()]
                rs2idx += 1
        elif rcmListing:
            # /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/xsardef/rcm/rcm1/SCLNC/GRD/2022/358/RCM1_OK2411022_PK2411799_1_SCLNC_20221224_125528_VV_VH_GRD/rcm1--owi-xx-20221224t125515-20221224t125759-_____-_____.nc
            data_str = owiFile.split("/")[-2]
            takeId = "_".join(data_str.split("_")[5:7])
            if takeId not in takeIdDict:
                takeIdDict[takeId] = {"comments": [], "paths": []}
            takeIdDict[takeId]["paths"].append(owiFile)
            takeIdDict[takeId]["comments"].append(comment)
        else:
            takeId = owiFile.split("/")[-1].split("-")[-1]
            if takeId not in takeIdDict:
                takeIdDict[takeId] = {}

            if "comments" not in takeIdDict[takeId]:
                takeIdDict[takeId]["comments"] = []
            takeIdDict[takeId]["comments"].append(comment.strip())

            if "paths" not in takeIdDict[takeId]:
                takeIdDict[takeId]["paths"] = []
            takeIdDict[takeId]["paths"].append(owiFile)

    reportFile = None
    fieldnames = ["Comments", "L2C file", "L2 files"]

    ### Creating/opening the reports files (concatReport.csv, outDir.txt and outUrl.txt) ###
    if reportPath:
        if os.path.exists(reportPath):
            reportFilepath = "%s/concatReport.csv" % reportPath
            try:
                reportFile = open(reportFilepath, "w+")
                logger.info("File %s created (CSV report file)" % reportFilepath)
            except IOError:
                logger.warn("Error: could not open report file: %s" % reportFilepath)

            writer = csv.DictWriter(reportFile, fieldnames=fieldnames)
            writer.writeheader()

            outDirReportFilePath = "%s/outDir.txt" % reportPath
            try:
                outDirReportFile = open(outDirReportFilePath, "w+")
                logger.info("File %s created (outDir report file)" % outDirReportFile)
            except IOError:
                logger.warn("Error: could not open report file: %s" % outDirReportFile)

            outUrlReportFilePath = "%s/outUrl.txt" % reportPath
            try:
                outUrlReportFile = open(outUrlReportFilePath, "w+")
                logger.info("File %s created (outDir report file)" % outUrlReportFile)
            except IOError:
                logger.warn("Error: could not open report file: %s" % outUrlReportFile)

    for takeId, dict in list(takeIdDict.items()):
        fullPath = generateFullPath(dict["paths"])
        commentStr = reorderComments(dict)
        # strip fullPath
        fullPathLst = fullPath.split("/")
        fullPath = "/".join(
            fullPathLst[fullPathLst.index("processings") + 3 :]
        )  # +3 : strip commit+config, as outDir know them
        fullPath = outDir + "/" + fullPath
        imgPaths = " ".join(dict["paths"])

        if reportPath:
            if reportFile:
                writer = csv.DictWriter(reportFile, fieldnames=fieldnames)
                writer.writerow(
                    {"Comments": commentStr, "L2C file": fullPath, "L2 files": imgPaths}
                )
            if outDirReportFile:
                outDirReportFile.write("%s\n" % fullPath)
            if outUrlReportFile:
                outUrlReportFile.write("%s\n" % toUrl(fullPath))

        oDir = os.path.dirname(fullPath)

        if (
            force
            or checkVersion(oDir)
            or checkStatus(oDir)
            or not os.path.isfile(fullPath)
        ):
            print("%s %s" % (fullPath, imgPaths))
        else:
            logger.debug("File %s already exists, skipping" % fullPath)

    if reportFile:
        reportFile.close()


def createConcatGeneric(l2paths):
    takeIdDict = {}
    rs2idx = 0

    for owiPath in l2paths:
        # FIXME these 2 checks are too weak
        rs2Listing = "RS2" in owiFileCom
        rcmListing = "rcm" in owiFileCom

        owiFile = os.path.basename(owiPath)

        if rs2Listing:
            owiSplit = owiFile.split("/")[-1].split("-")
            owiStartDate = owiSplit[-4]
            owiEndDate = owiSplit[-3]
            startDate = datetime.datetime.strptime(owiStartDate, "%Y%m%dt%H%M%S")
            endDate = datetime.datetime.strptime(owiEndDate, "%Y%m%dt%H%M%S")
            hasGroup = False
            for idx, dict in list(takeIdDict.items()):
                closestDelta = datetime.timedelta(999999999)
                if "startDates" in dict:
                    for sD in dict["startDates"]:
                        dateDelta = abs(endDate - sD)
                        if dateDelta < closestDelta:
                            closestDelta = dateDelta
                if "endDates" in dict:
                    for eD in dict["endDates"]:
                        dateDelta = abs(eD - startDate)
                        if dateDelta < closestDelta:
                            closestDelta = dateDelta
                if closestDelta.days == 0 and abs(closestDelta.seconds) < 15:
                    dict["startDates"].append(startDate)
                    dict["endDates"].append(endDate)
                    dict["paths"].append(owiPath)
                    hasGroup = True
                    break
            if not hasGroup:
                takeIdDict[rs2idx] = {}
                takeIdDict[rs2idx]["startDates"] = [startDate]
                takeIdDict[rs2idx]["endDates"] = [endDate]
                takeIdDict[rs2idx]["paths"] = [owiPath]
                rs2idx += 1
        elif rcmListing:
            # /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/xsardef/rcm/rcm1/SCLNC/GRD/2022/358/RCM1_OK2411022_PK2411799_1_SCLNC_20221224_125528_VV_VH_GRD/rcm1--owi-xx-20221224t125515-20221224t125759-_____-_____.nc
            data_str = owiFile.split("/")[-2]
            takeId = "_".join(data_str.split("_")[5:7])
            if takeId not in takeIdDict:
                takeIdDict[takeId] = {"comments": [], "paths": []}
            takeIdDict[takeId]["paths"].append(owiFile)
            takeIdDict[takeId]["comments"].append(comment)
        else:
            takeId = owiFile.split("/")[-1].split("-")[-1]
            if takeId not in takeIdDict:
                takeIdDict[takeId] = {}

            if "paths" not in takeIdDict[takeId]:
                takeIdDict[takeId]["paths"] = []
            takeIdDict[takeId]["paths"].append(owiPath)

    listReturn = []
    for takeId, dict in list(takeIdDict.items()):
        imgPaths = " ".join(dict["paths"])
        listReturn.append(imgPaths)

    return listReturn


def create_concat_from_safe(safe_paths):
    """
    Returns
    =======
    Path (not files) that can be concatenated, through that form
    ["path1 path2", "path1 path2 path3", ...]
    """
    takeIdDict = {}
    rs2idx = 0

    for safe_path in safe_paths:
        # FIXME these 2 checks are too weak
        rs2Listing = "RS2" in safe_path
        rcmListing = "RCM" in safe_path

        if rs2Listing:
            safe_split = safe_path.split("/")[-1].split("_")
            start_date = safe_split[-5]
            start_time = safe_split[-4]
            try:
                startDate = datetime.datetime.strptime(
                    start_date + "_" + start_time, "%Y%m%d_%H%M%S"
                )
            except ValueError as e:
                logger.warning(
                    f"Failed to parse acquisition time for {safe_path}. Skipping."
                )
                continue
            hasGroup = False
            for idx, dict in list(takeIdDict.items()):
                closestDelta = datetime.timedelta(999999999)
                if "startDates" in dict:
                    for sD in dict["startDates"]:
                        dateDelta = abs(startDate - sD)
                        if dateDelta < closestDelta:
                            closestDelta = dateDelta
                if closestDelta.days == 0 and abs(closestDelta.seconds) < 180:
                    dict["startDates"].append(startDate)
                    dict["paths"].append(safe_path)
                    hasGroup = True
                    break
            if not hasGroup:
                takeIdDict[rs2idx] = {}
                takeIdDict[rs2idx]["startDates"] = [startDate]
                takeIdDict[rs2idx]["paths"] = [safe_path]
                rs2idx += 1
        elif rcmListing:
            # /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/xsardef/rcm/rcm1/SCLNC/GRD/2022/358/RCM1_OK2411022_PK2411799_1_SCLNC_20221224_125528_VV_VH_GRD/rcm1--owi-xx-20221224t125515-20221224t125759-_____-_____.nc
            data_str = safe_path.split("/")[-1]
            takeId = "_".join(data_str.split("_")[5:7])
            if takeId not in takeIdDict:
                takeIdDict[takeId] = {"paths": []}
            takeIdDict[takeId]["paths"].append(safe_path)
        else:
            takeId = safe_path.split("/")[-1].split("_")[-2]
            if takeId not in takeIdDict:
                takeIdDict[takeId] = {}

            if "paths" not in takeIdDict[takeId]:
                takeIdDict[takeId]["paths"] = []
            takeIdDict[takeId]["paths"].append(safe_path)

    listReturn = []
    for takeId, dict in list(takeIdDict.items()):
        imgPaths = " ".join(dict["paths"])
        listReturn.append(imgPaths)

    return listReturn


def l2c_filename_from_l2_files(l2files):
    dates = {}
    for fname in l2files:
        try:
            startDate = datetime.datetime.strptime(
                os.path.basename(fname).split("-")[4], "%Y%m%dt%H%M%S"
            )
        except Exception as e:
            raise ValueError(
                "Error when parsing date in filename %s. Error : %s. Exiting"
                % (fname, str(e))
            )
        dates[startDate] = fname

    orderedDates = OrderedDict(sorted(dates.items()))

    l2files = list(orderedDates.values())

    fnData1 = os.path.basename(l2files[0]).split("-")
    fnData2 = os.path.basename(l2files[-1]).split("-")
    fname = "%s-%s-%s-cc-%s-%s-%s-%s" % (
        fnData1[0],
        fnData1[1],
        fnData1[2],
        fnData1[4],
        fnData2[5],
        fnData1[6],
        fnData1[7],
    )

    return fname

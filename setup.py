from setuptools import setup 

setup(name='sarwingL2C',
      description='Sarwing concatenation module',
      url='https://gitlab.ifremer.fr/sarwing/sarwingL2C.git',
      use_scm_version={'write_to': '%s/_version.py' % "sarwingL2C"},
      author = "Theo Cevaer",
      author_email = "Theo.Cevaer@ifremer.fr",
      license='GPL',
      packages=['sarwingL2C'],
      install_requires=[
          'numpy', 'shapely', 'netCDF4',
          'pathurl @ git+https://gitlab.ifremer.fr/sarwing/pathurl.git'
      ],
      zip_safe=False,
      scripts=['bin/swConcatWrapper.sh',
               'bin/swConcat.py',
               'bin/swGroup.py',
               'bin/swConcatListing.py'],
)

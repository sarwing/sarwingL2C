import unittest
import sarwingL2C.swConcatProcessor as concatProc
import os


class ConcatTest(unittest.TestCase):
    def test_concat(self):
        data_dir = os.path.dirname(__file__)
        files = ["s1a-iw-owi-xx-20230901t103347-20230901t103416-050133-060890.nc",
                 "s1a-iw-owi-xx-20230901t103416-20230901t103441-050133-060890.nc",
                 "s1a-iw-owi-xx-20230901t103441-20230901t103506-050133-060890.nc"]
        files = [os.path.join(data_dir, f) for f in files]
        status = concatProc.netCDFConcat(files, "/tmp/", True, False)
        self.assertTrue(status == 0)


if __name__ == '__main__':
    unittest.main()

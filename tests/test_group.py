import unittest
import sarwingL2C.swGroupProcessor as gp
import os

from io import StringIO
import sys


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout


class GroupTest(unittest.TestCase):
    def test_group_s1(self):
        # /home/datawork-cersat-public/cache/project/sarwing/conda_sarwingL2X/bin/swConcat.py -d /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/IW/S1A_IW_OWIH_1S/2023/244/S1A_IW_OWIH_CC_20230901T103347_20230901T103506_050133_060890/s1a-iw-owi-cc-20230901t103347-20230901t103506-050133-060890.nc -i /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L1/IW/S1A_IW_GRDH_1S/2023/244/S1A_IW_GRDH_1SDV_20230901T103441_20230901T103506_050133_060890_47EE.SAFE/s1a-iw-owi-xx-20230901t103441-20230901t103506-050133-060890.nc /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L1/IW/S1A_IW_GRDH_1S/2023/244/S1A_IW_GRDH_1SDV_20230901T103416_20230901T103441_050133_060890_DA9A.SAFE/s1a-iw-owi-xx-20230901t103416-20230901t103441-050133-060890.nc /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L1/IW/S1A_IW_GRDH_1S/2023/244/S1A_IW_GRDH_1SDV_20230901T103347_20230901T103416_050133_060890_6194.SAFE/s1a-iw-owi-xx-20230901t103347-20230901t103416-050133-060890.nc
        # -d /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2023/146/S1A_EW_OWIM_CC_20230526T090903_20230526T091031_048703_05DB7B/s1a-ew-owi-cc-20230526t090903-20230526t091031-048703-05DB7B.nc -i /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L1/EW/S1A_EW_GRDM_1S/2023/146/S1A_EW_GRDM_1SDV_20230526T091008_20230526T091031_048703_05DB7B_D388.SAFE/s1a-ew-owi-xx-20230526t091008-20230526t091031-048703-05DB7B.nc /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L1/EW/S1A_EW_GRDM_1S/2023/146/S1A_EW_GRDM_1SDV_20230526T090903_20230526T091008_048703_05DB7B_1545.SAFE/s1a-ew-owi-xx-20230526t090903-20230526t091008-048703-05DB7B.nc
        data_dir = os.path.dirname(__file__)
        files = ["/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/"
                 "sentinel-1a/L1/IW/S1A_IW_GRDH_1S/2023/244/S1A_IW_GRDH_1SDV_20230901T103347_"
                 "20230901T103416_050133_060890_6194.SAFE/s1a-iw-owi-xx-20230901t103347-"
                 "20230901t103416-050133-060890.nc comment",
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a"
                 "/L1/IW/S1A_IW_GRDH_1S/2023/244/S1A_IW_GRDH_1SDV_20230901T103416_"
                 "20230901T103441_050133_060890_DA9A.SAFE/s1a-iw-owi-xx-20230901t103416-"
                 "20230901t103441-050133-060890.nc comment",
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default"
                 "/sentinel-1a/L1/IW/S1A_IW_GRDH_1S/2023/244/"
                 "S1A_IW_GRDH_1SDV_20230901T103441_20230901T103506_050133_060890_47EE.SAFE/"
                 "s1a-iw-owi-xx-20230901t103441-20230901t103506-050133-060890.nc comment",
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/"
                 "sentinel-1a/L1/EW/S1A_EW_GRDM_1S/2023/146/S1A_EW_GRDM_1SDV_20230526T091008_20230526T091031_"
                 "048703_05DB7B_D388.SAFE/s1a-ew-owi-xx-20230526t091008-20230526t091031-048703-05DB7B.nc comment",
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/"
                 "sentinel-1a/L1/EW/S1A_EW_GRDM_1S/2023/146/S1A_EW_GRDM_1SDV_20230526T090903_20230526T091008"
                 "_048703_05DB7B_1545.SAFE/s1a-ew-owi-xx-20230526t090903-20230526t091008-048703-05DB7B.nc comment"]
        files = [os.path.join(data_dir, f) for f in files]

        with Capturing() as output:
            gp.createConcat(files, outDir="/tmp", force=False, reportPath="/tmp", debug=True)
        line1 = ("/tmp/sentinel-1a/L2C/IW/S1A_IW_OWIH_1S/2023/244"
                 "/S1A_IW_OWIH_CC_20230901T103347_20230901T103506_050133_060890/s1a-iw-owi-cc-20230901t103347"
                 "-20230901t103506-050133-060890.nc "
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel"
                 "-1a/L1/IW/S1A_IW_GRDH_1S/2023/244"
                 "/S1A_IW_GRDH_1SDV_20230901T103347_20230901T103416_050133_060890_6194.SAFE/s1a-iw-owi-xx"
                 "-20230901t103347-20230901t103416-050133-060890.nc "
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel"
                 "-1a/L1/IW/S1A_IW_GRDH_1S/2023/244"
                 "/S1A_IW_GRDH_1SDV_20230901T103416_20230901T103441_050133_060890_DA9A.SAFE/s1a-iw-owi-xx"
                 "-20230901t103416-20230901t103441-050133-060890.nc "
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel"
                 "-1a/L1/IW/S1A_IW_GRDH_1S/2023/244"
                 "/S1A_IW_GRDH_1SDV_20230901T103441_20230901T103506_050133_060890_47EE.SAFE/s1a-iw-owi-xx"
                 "-20230901t103441-20230901t103506-050133-060890.nc")

        line2 = ("/tmp/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2023/146"
                 "/S1A_EW_OWIM_CC_20230526T090903_20230526T091031_048703_05DB7B/s1a-ew-owi-cc-20230526t090903"
                 "-20230526t091031-048703-05DB7B.nc "
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel"
                 "-1a/L1/EW/S1A_EW_GRDM_1S/2023/146"
                 "/S1A_EW_GRDM_1SDV_20230526T091008_20230526T091031_048703_05DB7B_D388.SAFE/s1a-ew-owi-xx"
                 "-20230526t091008-20230526t091031-048703-05DB7B.nc "
                 "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel"
                 "-1a/L1/EW/S1A_EW_GRDM_1S/2023/146"
                 "/S1A_EW_GRDM_1SDV_20230526T090903_20230526T091008_048703_05DB7B_1545.SAFE/s1a-ew-owi-xx"
                 "-20230526t090903-20230526t091008-048703-05DB7B.nc")
        self.assertTrue(output[0] == line1 and output[1] == line2)


if __name__ == '__main__':
    unittest.main()
